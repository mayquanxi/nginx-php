#pull ubuntu 18.04
FROM ubuntu:18.04

#update
RUN apt-get update

#install nginx
RUN apt-get install -y nginx

#install new php
RUN  \
	FRONTEND=noninteractive && \
	apt-get install -y php-fpm \
		php-mysql
#config nginx
COPY default2 /etc/nginx/sites-available/default2
RUN ln -s /etc/nginx/sites-available/default2 /etc/nginx/sites-enabled/
RUN mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
RUN rm /etc/nginx/sites-enabled/default
RUN echo "<?php phpinfo(); ?>" >> /var/www/html/info.php
COPY php-connect-mysql.php /var/www/html/mysql.php
#start nginx
COPY CMD.sh ./CMD.sh
RUN chmod 755 ./CMD.sh
CMD ./CMD.sh

